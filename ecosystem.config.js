module.exports = {
  apps: [
    {
      name: 'blog_app_server',
      script: 'server.js',
      // args: 'start',
      time: true,
      exec_mode: 'fork',
      instances: 1,
      autorestart: true,
      watch: true,
      ignore_watch: ['node_modules'],
      max_memory_restart: '200M',
      max_restarts: 5, // number of consecutive unstable restarts
      min_uptime: 5000, // min uptime of the app to be considered started
      restart_delay: 1000 // time to wait before restarting a crashed app
    },
  ],
};
