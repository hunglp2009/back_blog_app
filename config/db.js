const mongoose = require("mongoose");
require('dotenv').config();
// const dbUrl = process.env.DB_URL;
const host = process.env.DB_HOST;
const port = process.env.DB_PORT;
const dbName = process.env.DB_NAME;
const user = process.env.DB_USER;
const pass = process.env.DB_PASS;

const dbUrl = `mongodb://${user}:${pass}@${host}:${port}/${dbName}?authMechanism=DEFAULT&authSource=admin`;
// console.log(dbUrl);
mongoose.connect(dbUrl).then(() => {
    console.log("connected!");
}).catch((err) => {
    console.log(err);
})
