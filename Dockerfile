# PREPARE EVIREMENT NODEJS, VERSION node18/alpine
FROM node:18-alpine

# PLACE SAVE SOURCE CODE
WORKDIR /blog_app/backend

RUN npm install pm2@latest -g

# A wildcard is used to ensure both package.json AND package-lock.json are copied 
COPY package*.json ./

RUN npm install --silent

# COPY app source
COPY . .

# BUILD SOURCE CODE
# RUN npm run build-src

EXPOSE 5000

# CMD [ "npm", "start" ]
CMD ["pm2-runtime", "ecosystem.config.js"]
# CMD ["pm2", "start", "ecosystem.config.js"]

# docker build --tag genealogy-docker .
# docker run -p 3000:3000 -d genealogy-docker